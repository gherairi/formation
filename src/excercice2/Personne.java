package excercice2;

public class Personne {

	
	// attribue
	String nom;
	String prenom;
	int age;

	// constructeur pour instancier la classe 
	void Personne() {
	}

	// ------ Getters/setters .......
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	// -----------------------

	// fonctions
	public void afficherPersonne() {
		System.out.println( nom + " " + prenom  + " " + age);
	}

}
