package exercice1;

import java.util.Scanner;

public class Calculatrice {
	
	public void calcul() {
		Integer chiffre1 = lireEntier();
		Integer chiffre2 = lireEntier();
		String operateur = lireOperation();
		int resultat;
		if (chiffre1 != null && chiffre2 != null) {
			switch (operateur) {
			case "+":
				resultat = chiffre1 + chiffre2;
				affichage("Opération d'addition: " + chiffre1 + " + " + chiffre2 + " = " + resultat);
				break;
			case "-":
				resultat = chiffre1 - chiffre2;
				affichage("Opération de soustraction: " + chiffre1 + " - " + chiffre2 + " = " + resultat);
				break;
			case "*":
				resultat = chiffre1 * chiffre2;
				affichage("Opération de multiplication: " + chiffre1 + " * " + chiffre2 + " = " + resultat);
				break;
			case "/":
				resultat = chiffre1 / chiffre2;
				affichage("Opération de division: " + chiffre1 + " / " + chiffre2 + " = " + resultat);
				break;
			} 
			
		}else {
			System.out.println("L'un des chiffres est Null");
		}
		
		
	}
	
	private  String lireOperation() {
		Scanner scanner = new Scanner(System.in);
		// Lecture du premier chiffre
		affichage("Veuillez saisir un Opération : ");
		String operateur = scanner.nextLine();
		return operateur;
	}

	private  Integer lireEntier() {
		Scanner scanner = new Scanner(System.in);
		// Lecture du premier chiffre
		affichage("Veuillez saisir un chiffre : ");
		String chiffre = scanner.nextLine();
		// Vérification du premier chiffre
		boolean entierTest = verificationEntier(chiffre);

		if (entierTest) {
			return Integer.parseInt(chiffre);
		} else {
			return null;
		}
	}

	private  boolean verificationEntier(String chaine) {
		try {
			Integer.parseInt(chaine);
			return true;
		} catch (NumberFormatException e) {
			System.out.println("le chiffre entré n'est pas un entier");
			return false;
		}
	}

	private  void affichage(String texte) {
		System.out.println(texte);
	}

}
